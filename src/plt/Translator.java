package plt;

import java.util.regex.Pattern;

public class Translator {
	
	public static final String NIL = "nil";
	
	private String phrase;

	public Translator(String inputPhrase) {

		this.phrase = inputPhrase;
	}

	public String getPhrase() {

		return this.phrase;
	}

	public String translate() {
		
		if( !checkForbiddenPunctuation() ) {
			return "Error. Forbidden Punctuation"; 
		}
		
		if(this.phrase.contains(" ") || this.phrase.contains("-")) {
			
			int j=0;
			String partiallyTranslated = "";
			String[] words;
			
			if(this.phrase.contains(" ")) {		
				words = this.phrase.split(" ");
			} else {
				words = this.phrase.split("-");
			}
			
			while (j < words.length) {
				
				char firstPunt = getFirstPuntuationInWord(words[j]);
				char lastPunt = getLastPuntuationInWord(words[j]);
				
				if(firstPunt != 0) {
					words[j] = words[j].substring(1);
				}
				
				if(lastPunt != 0) {
					int index = words[j].indexOf(lastPunt);
					words[j] = words[j].substring(0, index);
				}
				
				if(startsWithVowel(words[j])) {
					if(words[j].endsWith("y")) {
						words[j] = words[j] + "nay";
					} else if(endsWithVowel(words[j])) {
						words[j] = words[j] + "yay";
					} else if(!endsWithVowel(words[j])) {
						words[j] = words[j] + "ay";
					}
				}
				else if(this.phrase.equals("")){
					words[j] = NIL;
						
				} else {
					if(isConsonantByIndex(1, words[j])) {
						
						String consonants = "";
						int i=0;
						while(i<words[j].length() && isConsonantByIndex(i, words[j])) {
							consonants = consonants + words[j].charAt(i);
							i++;
						}
						
						words[j] = words[j].substring(i);
						words[j] = words[j] + consonants + "ay";	
						
						
					} else {
						//Starts with single consonant
						char first = words[j].charAt(0);
						words[j] = words[j].substring(1);
						words[j] = words[j] + first + "ay";

					}
					
				}
				
				if(lastPunt != 0 && firstPunt != 0) {
					partiallyTranslated = firstPunt + partiallyTranslated + words[j] + lastPunt;
				}
				else if(lastPunt != 0) {
					partiallyTranslated = partiallyTranslated + words[j] + lastPunt;
				}
				else if(firstPunt != 0) {
					partiallyTranslated = firstPunt + partiallyTranslated + words[j];
				}
				else {
					partiallyTranslated = partiallyTranslated + words[j];
				}
				
				j++;
				if(j < words.length) {
					if(this.phrase.contains(" ")) {
						partiallyTranslated = partiallyTranslated + " ";
					} else {
						partiallyTranslated = partiallyTranslated + "-";
					}
				}		
			}
			
			this.phrase = partiallyTranslated;
			return this.phrase;
			
		}
		else if(this.phrase.equals("")) {
			return NIL;
		}
		else {			
			char firstPunt = getFirstPuntuationInWord(this.phrase);
			char lastPunt = getLastPuntuationInWord(this.phrase);
			
			if(firstPunt != 0) {
				this.phrase = this.phrase.substring(1);
			}
			
			if(lastPunt != 0) {
				int index = this.phrase.indexOf(lastPunt);
				this.phrase = this.phrase.substring(0, index);
			}
			
			//Phrase with no space 
			if(startsWithVowel()) {
				if(this.phrase.endsWith("y")) {
					this.phrase = this.phrase + "nay";
				} else if(endsWithVowel()) {
					this.phrase = this.phrase + "yay";
				} else if(!endsWithVowel()) {
					this.phrase = this.phrase + "ay";
				}
			}
			else if(this.phrase.equals("")){
				return NIL;
					
			} else {
				if(isConsonantByIndex(1)) {
					
					String consonants = "";
					int i=0;
					while(i<this.phrase.length() && isConsonantByIndex(i)) {
						consonants = consonants + this.phrase.charAt(i);
						i++;
					}
					
					this.phrase = this.phrase.substring(i);
					this.phrase = this.phrase + consonants + "ay";	
					
					
				} else {
					//Starts with single consonant
					char first = this.phrase.charAt(0);
					this.phrase = this.phrase.substring(1);
					this.phrase = this.phrase + first + "ay";

				}
				
			}
			
			if(lastPunt != 0 && firstPunt != 0) {
				return this.phrase = firstPunt + this.phrase + lastPunt;
			}
			else if(lastPunt != 0) {
				return this.phrase = this.phrase + lastPunt;
			}
			else if(firstPunt != 0) {
				return this.phrase = firstPunt + this.phrase;
			}
			else {
				return this.phrase;
			}
		
		}		

	}
	
	private boolean isConsonantByIndex(int i) {
		
		return this.phrase.charAt(i) != 'a' && this.phrase.charAt(i) != 'e' && this.phrase.charAt(i) != 'i'
				&& this.phrase.charAt(i) != 'o' && this.phrase.charAt(i) != 'u' ? true : false;

	}
	
	private boolean isConsonantByIndex(int i, String word) {
		
		return word.charAt(i) != 'a' && word.charAt(i) != 'e' && word.charAt(i) != 'i'
				&& word.charAt(i) != 'o' && word.charAt(i) != 'u' ? true : false;

	}

	private boolean startsWithVowel() {
		
		return this.phrase.startsWith("a") || this.phrase.startsWith("e") || this.phrase.startsWith("i") || this.phrase.startsWith("o") || this.phrase.startsWith("u") ? true : false;	
	}
	
	private boolean endsWithVowel() {
		
		return this.phrase.endsWith("a") || this.phrase.endsWith("e") || this.phrase.endsWith("i") || this.phrase.endsWith("o") || this.phrase.endsWith("u") ? true : false;	
	}
	
	private boolean startsWithVowel(String word) {
		
		return word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u") ? true : false;	
	}
	
	private boolean endsWithVowel(String word) {
		
		return word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") || word.endsWith("u") ? true : false;	
	}
	
	private char getLastPuntuationInWord(String word) {
		
		return word.charAt(word.length() -1 ) == '.' || word.charAt(word.length() -1 ) == ',' || word.charAt(word.length() -1 ) == ';' ||
				word.charAt(word.length() -1 ) == ':' || word.charAt(word.length() -1 ) == '\'' || word.charAt(word.length() -1 ) == '?' ||
				word.charAt(word.length() -1 ) == '!' || word.charAt(word.length() -1 ) == '(' || word.charAt(word.length() -1 ) == ')' 
				? word.charAt(word.length() -1 ) : 0;
	}
	
	private char getFirstPuntuationInWord(String word) {
		
		return word.charAt(0) == '.' || word.charAt(0) == ',' || word.charAt(0) == ';' ||
				word.charAt(0) == ':' || word.charAt(0) == '\'' || word.charAt(0) == '?' ||
				word.charAt(0) == '!' || word.charAt(0) == '(' || word.charAt(0) == ')'
				? word.charAt(0) : 0;
	}
	
	private boolean checkForbiddenPunctuation() {
		
		if(this.phrase.contains(".") ||this.phrase.contains(",") || this.phrase.contains(";") ||
				this.phrase.contains(":") || this.phrase.contains("\'") ||this.phrase.contains("?") ||
				this.phrase.contains("!") || this.phrase.contains("(") || this.phrase.contains(")") || this.phrase.contains("-") ) {
			return true;
		} else {
			for(int i=0; i<this.phrase.length(); i++) {
				
				String temp = Character.toString( this.phrase.charAt(i) );
				
				if( Pattern.matches("\\p{Punct}", temp) ) {
					return false;
				}
				 
			}
			
			return true;
		} 
	}
	
}
