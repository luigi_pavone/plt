package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "Hello world";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("Hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);	
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationStartingVowelEndingY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationStartingUEndingY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationStartingVowelEndingVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationStartingVowelEndingConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationStartingSingleConsonant1() {
		String inputPhrase = "fello";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("ellofay", translator.translate());
	}
	
	@Test
	public void testTranslationStartingMoreConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithWhiteSpace() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithCompositeWords() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	// . , ; : ? ! ' ( )
	
	@Test
	public void testTranslationPhraseWithPunctuationsOnlyEnd() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithPunctuationsOnlyEndAndCompositeWords() {
		String inputPhrase = "hello-world!";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("ellohay-orldway!", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithPunctuationsStartAndEnd() {
		String inputPhrase = "(hello world)";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("(ellohay orldway)", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithPunctuationsInBeetween() {
		String inputPhrase = "hello' world?";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("ellohay' orldway?", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithPunctuationsStartInBeetweenEnd() {
		String inputPhrase = "(hello: world.";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("(ellohay: orldway.", translator.translate());
	}
	
	@Test
	public void testTranslationSingleWordWithPunctuationsEnd() {
		String inputPhrase = "hello?";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("ellohay?", translator.translate());
	}
	
	@Test
	public void testTranslationSingleWordWithPunctuationsStart() {
		String inputPhrase = ":apple";
		Translator translator = new Translator(inputPhrase);	
		assertEquals(":appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationSingleWordWithPunctuationsStartEnd() {
		String inputPhrase = "(known)";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("(ownknay)", translator.translate());
	}
	
	@Test
	public void testTranslationWithForbiddenPunctuation() {
		String inputPhrase = "<hello world>";
		Translator translator = new Translator(inputPhrase);	
		assertEquals("Error. Forbidden Punctuation", translator.translate());
	}
	
}
